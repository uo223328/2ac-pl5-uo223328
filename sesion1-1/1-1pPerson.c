#include <stdio.h>
#include <string.h>

struct _Person
{
    char name[30];
    int heightcm;
    double weightkg;
};
// This abbreviates the type name
typedef struct _Person Person;
typedef Person* PPerson;
int main(int argc, char* argv[])
{
    Person Peter;
    
    PPerson pPeter;
    pPeter = &Peter;
    Peter.heightcm = 180;
    Peter.weightkg = 84.0;
    pPeter->weightkg = 83.2;
    strcpy(Peter.name, "Peter");
    Peter.heightcm = 175;

    // Assign the weight
   Peter.weightkg = 78.7;
    // Show the information of the Peter data structure on the screen
   printf("%s %d %f",Peter.name,Peter.heightcm,Peter.weightkg);
    return 0;
}
